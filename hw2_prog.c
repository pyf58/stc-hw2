#include <stdio.h>

double Midpoint(double x_start, double x_end, int n);
double Trapezoid(double x_start, double x_end, int n);
double Simpson(double x_start, double x_end, int n);

main()
{
    int n;
    double standard = 0.125;
		
		/* Midpoint */
		
    printf("\"Midpoint Rule\"\n");
    for (n = 10, standard ; n < 100001; n = n * 10 )
    {
        printf("n = %i, error = %f\n", n ,\
				fabs( Midpoint( 0, 1, n ) - standard ));
    }
		
		/* Trapezoid */
		
    printf("\"Trapezoid rule\"\n");
    for (n = 10, standard ; n < 100001; n = n * 10 )
    {
        printf("n = %i, error = %f\n", n,\
				fabs( Trapezoid( 0, 1, n ) - standard ));
    }
		
		/* Simpson */
		
    printf("\"Simpson rule\"\n");
    for (n = 10, standard ; n<100001; n = n * 10 )
    {
        printf("n = %i, error = %f\n" , n ,\
				fabs( Simpson( 0, 1, n ) - standard ));
    }
}