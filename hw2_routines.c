#include "hw2_routines.h"

double Midpoint(double x_start, double x_end, int n)
{
    int i;
    double sum, result = 0, dx;
    dx = (x_end - x_start)/n;

    for (i = 0; i < n; i++)
    {
        sum += fn( (x_start + i * dx)+ dx/2 );
    }
    result = dx * sum;
    return result;
}

double Trapezoid(double x_start,double x_end,int n)
{
    int i;
    double sum = 0, result = 0, dx;
    dx = (x_end - x_start)/n;
    for (i = 1; i <= n - 1; i++)
{
    sum += fn( x_start + i * dx );
}
result = (fn (x_start) + fn (x_end) + 2 * sum) * dx/2;
return result;
}

double Simpson(double x_start,double x_end, int n)
{
    int i;
    double sum, result = 0, dx;
    dx = (x_end - x_start)/n;
    for (i = 1; i < n; i += 2)
    {
        sum += (fn( ( x_start + (i-1) * dx) ) +\
				4 * fn( x_start + i * dx ) + fn( x_start + (i+1) * dx ));
    }
    result = (dx/3) * sum;
    return result;
}