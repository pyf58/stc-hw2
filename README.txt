Priscilla Yumiko Fujikawa

When calling the midpoint function, f(x) = x^7 is called
5 times, since I have a for loop that computes 5 different values of n.
Moreover, in the midpoint function itself, f(x) is called only one time.

When calling the trapezoid function, f(x) = x^7 is called
15 times, since I have a for loop that computes 5 different values of n.
Moreover, in the trapezoid function itself, f(x) is called three times.

When calling the simpson function, f(x) = x^7 is called
15 times, since I have a for loop that computes 5 different values of n.
Moreover, in the midpoint function itself, f(x) is called three time.

In total, f(x) is called 35 times.

In order to optimize the program, the optimization level in the Makefile
could be changed, or the function could be changed. For example,
x^7 = (x^2)*(x^2)*(x^2)*x.